package lab2.bookstore;
import lab2.products.*;

public class ProductInformation {
		private Product product;
		private int price;
		private int numberOfItemsAvailable;
	public ProductInformation(Product product, int price, int numberOfItemsAvailable)
		{
			this.product = product;
			this.price   = price;
			this.numberOfItemsAvailable = numberOfItemsAvailable;
		}
	public Product getProduct()
	{
		return this.product;
	}
	public int getPrice()
	{
		return this.price;
	}
	public void setPrice(int price)
	{
		this.price = price;
	}
	public int getNumberOfItemsAvailable()
	{
		return this.numberOfItemsAvailable;
	}
	public void setNumberOfItemsAvailable(int numberOfItemsAvailable)
	{
		this.numberOfItemsAvailable = numberOfItemsAvailable;
	}
}
