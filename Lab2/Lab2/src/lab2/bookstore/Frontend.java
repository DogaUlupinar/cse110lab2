package lab2.bookstore;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import lab2.products.*;


public class Frontend implements IFrontend {
		protected Store ourstore;
	public Frontend(Store store)
	{
		this.ourstore = store;
	}
	public Set<ProductInformation> searchProduct(String nameOfProduct)
	{
		HashSet<ProductInformation> store_productinfo = new HashSet<ProductInformation>();
		Set<Product> store_products = ourstore.database.keySet();
		for (Product p: store_products)
		{
			if (nameOfProduct.equals(p.getName()))
			{
				store_productinfo.add(ourstore.database.get(p));
			}
		}
		return store_productinfo;
	}
	public void addToCart(Product product, Cart cart)
	{
		cart.addItem(product);
	}
	public void removeFromCart(Product product, Cart cart)
	{
		cart.removeItem(product);
	}
	public int totalPrice(Cart cart)
	{
		return cart.getTotalPrice();
	}
	public List<Product> getCartContent(Cart cart)
	{
		List<Product> product_list = new ArrayList<Product>();
		for (Product P: cart.getContent())
		{
			product_list.add(P);
		}
		return product_list;
				
	}
	public void checkout(String Name, String addressOfDelivery, CreditCard card, Cart cart) throws BookstoreException
	{
		// check the products in the cart are in the store
		Product[] cart_content = cart.getContent();
		
		for (Product p : cart_content)
		{
			if (!ourstore.database.containsKey(p))
			{
				throw new BookstoreException();
			}
		}
		//Authorize credit cart payment
		ourstore.bank.authorizePayment(card,cart.getTotalPrice());
		//Update product availability
		for (Product p : cart_content)
		{
			ourstore.getBackend().remove(p);
		}
	}
}
