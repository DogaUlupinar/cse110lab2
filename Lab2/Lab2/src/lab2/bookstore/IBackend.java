package lab2.bookstore;

import lab2.products.Product;

public interface IBackend {

	void addInformation(Product product, int price) throws BookstoreException;
	void add(Product product) throws BookstoreException;
	void remove(Product product)throws BookstoreException;
	ProductInformation lookup(Product product);
}