package lab2.bank;

import lab2.bookstore.CreditCard;

public interface IBank {
	public boolean authorizePayment(CreditCard card, int price);
}

