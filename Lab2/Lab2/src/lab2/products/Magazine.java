package lab2.products;

public class Magazine extends Product{
		private int weekNumber;
	public Magazine(String name, String description, int weekNumber)
	{
		super.name=name;
		super.description=description;
		this.weekNumber=weekNumber;
	}
	public int getWeekNumber()
	{
		return this.weekNumber;
	}
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = result* prime +this.weekNumber;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Magazine other = (Magazine) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (weekNumber == 0){
			if (other.getWeekNumber() != 0)
				return false;
		}else if (this.weekNumber!=other.getWeekNumber())
			return false;
		return true;
	}

}
