package lab2.products;

public class Book extends Product{
		private int pageNumber;
	public Book(String name, String description, int pageNumber)
	{
		super.name=name;
		super.description=description;
		this.pageNumber=pageNumber;
	}
	public int getpageNumber()
	{
		return this.pageNumber;
	}
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pageNumber== 0){
			if (other.getpageNumber() != 0)
				return false;
		}else if (this.pageNumber!=other.getpageNumber())
			return false;
		return true;
	}
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result+this.pageNumber;
		return result;
	}
	
}
